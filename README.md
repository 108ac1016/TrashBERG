# 大二上互動設計專題


##  專題名稱：TrashCream

#### 組員: 林辰、廖祐凜、宋明彥、劉家仁、彭裔甄

### 裝置說明:

按壓裝置上的按鈕來產生垃圾，收集到一定程度後進入加工廠場景。
透過按鍵控制廠內的裝置將垃圾加工成冰淇淋。

### 技術說明：

* Unity 2019.4
* Arduino

### 遊戲畫面 
##### 點擊圖片播放

[![](https://gitlab.com/108ac1016/TrashBERG/-/raw/main/Recordings/Trashcream-screenshot.png)](https://gitlab.com/108ac1016/TrashBERG/-/blob/main/Recordings/%E5%A4%A7%E4%BA%8C_Trashcream.mp4)

[![](https://gitlab.com/108ac1016/TrashBERG/-/raw/main/Recordings/Trashcream-screenshot_2.png)](https://gitlab.com/108ac1016/TrashBERG/-/blob/main/Recordings/movie_011.mp4)
