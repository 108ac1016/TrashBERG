﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackHole : MonoBehaviour
{
    public int Count;
    
    void Start()
    {
        Count = 0;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "ModelA" || other.gameObject.tag == "ModelB")
        {
            other.gameObject.SetActive(false);
            Count++;
        }
    }
}
