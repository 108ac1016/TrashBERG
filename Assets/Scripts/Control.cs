﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Control : MonoBehaviour,IPointerDownHandler,IPointerUpHandler
{
    private float downTime;

    public void OnPointerDown(PointerEventData eventData)
    {
        Generation.isPressed = true;
        this.downTime = Time.realtimeSinceStartup;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        Generation.isPressed = false;
    }

    void Update()
    {
        if(downTime > Time.realtimeSinceStartup + 2f)
        {
            Generation.isPressed = false;
            Debug.Log("LongPress");
        }
    }
}
