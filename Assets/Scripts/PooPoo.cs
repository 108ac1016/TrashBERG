﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PooPoo : MonoBehaviour
{
    public GameObject O_Poo, E_Poo, D_Poo;
    bool OneShitOneDay;
    // Start is called before the first frame update
    void Start()
    {
        OneShitOneDay = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(Pipe.stage == 4)
        {
            if (!OneShitOneDay)
            {
                OneShitOneDay = true;
                switch (Connect.final)
                {
                    case 1:
                        Instantiate(O_Poo, this.transform.position, Quaternion.identity);
                        break;
                    case 2:
                        Instantiate(E_Poo, this.transform.position, Quaternion.identity);
                        break;
                    case 3:
                        Instantiate(D_Poo, this.transform.position, Quaternion.identity);
                        break;
                }
            }
            
        }
    }
}
