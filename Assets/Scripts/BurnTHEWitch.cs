﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurnTHEWitch : MonoBehaviour
{
    public static bool Stop;
    Renderer Trash_Mat;
    // Start is called before the first frame update
    void Start()
    {
        Stop = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Pipe.stage == 3)
        {
            Stop = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!Stop)
        {
            if (other.gameObject.tag == "ModelA" || other.gameObject.tag == "ModelB")
            {
                Trash_Mat = other.GetComponent<Renderer>();
                Trash_Mat.material.SetColor("_Color", Color.black);
            }
        }
    }
}
