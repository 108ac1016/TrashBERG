﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OP : MonoBehaviour
{
    Rigidbody RB;
    Animation Anim;
    // Start is called before the first frame update
    void Start()
    {
        RB = this.gameObject.GetComponent<Rigidbody>();
        Anim = this.gameObject.GetComponent<Animation>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Ground")
        {
            RB.isKinematic = true;
            switch (this.tag)
            {
                case "Ocean":
                    Anim.Play("OPoo_Anim");
                    Pipe.stage = 5;
                    break;
                case "Electric":
                    Anim.Play("EPoo_Anim");
                    Pipe.stage = 5;
                    break;
                case "Disposable":
                    Anim.Play("DPoo_Anim");
                    Pipe.stage = 5;
                    break;
            }
        }
    }
}
