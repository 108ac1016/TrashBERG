﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hand : MonoBehaviour
{
    public GameObject[] Hands;
    Animation Hand_Anim;
    bool Stop;

    public static bool One, Two, Three;
    // Start is called before the first frame update
    void Start()
    {
        Stop = true;
        One = false;
        Two = false;
        Three = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (One && Two && Three)
        {
            Stop = true;
        }
        if (Pipe.stage == 3)
        {
            Stop = false;
        }
        if (!Stop)
        {
            if(Connect.readi == 1)
            {
                Hand_Anim = Hands[0].GetComponent<Animation>();
                Hand_Anim.Play("Hand1");
                One = true;
            }
            else if (Connect.readi == 2)
            {
                Hand_Anim = Hands[1].GetComponent<Animation>();
                Hand_Anim.Play("Hand2");
                Two = true;
            }
            else if (Connect.readi == 3)
            {
                Hand_Anim = Hands[2].GetComponent<Animation>();
                Hand_Anim.Play("Hand3");
                Three = true;
            }
        }
    }
}
