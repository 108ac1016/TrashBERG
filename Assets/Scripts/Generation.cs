﻿using System.Collections;
using System.Collections.Generic;
using System.IO.Ports;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UIElements;
using UnityEngine.SceneManagement;


public class Generation : MonoBehaviour
{
    public GameObject[] Trash_Ocean, Trash_Electric, Trash_Disposable; // ocean=0,electric=1,onetime=2,random=3
    public Transform Bag;
    public Transform Generate_Spot; // 垃圾生成點
    public Button GenerateButton;

    GameObject[] Trash, Die;

    int O, E, D, Count, BagCount, BagSave, Cleaner; // OED = 123 / Count 垃圾數 / BagCount 垃圾袋數 / BagSave 垃圾袋暫存 / Cleaner 垃圾清除
    public static int i;
    Vector3 Range = new Vector3(Random.Range(-2, 2), 0, Random.Range(-2, 2)); // 隨機點
    public static bool isPressed; // 按壓檢測
    public static int Total; // 垃圾總數
    bool CleanerActive; // 自動清理開關

    GameObject[] NotDying, Score;
    float delay; // 生產延遲
    public int Gspeed;

    void Start()
    {
        O = Trash_Ocean.Length;
        E = Trash_Electric.Length;
        D = Trash_Disposable.Length;
        i = 3;
        Count = 0;
        BagCount = 0;
        BagSave = 0;
        Cleaner = 1;
        CleanerActive = false;
        Gspeed = 1;
        Total = 0;
    }


    void Update()
    {
        InputTF();
        TrashTotal(Total);
        if (isPressed)
        {
            if (BagCount == 0 || BagSave == BagCount)
            {
                BagCount++;
                if (BagCount > 9) // 垃圾袋 > 9 自動清除最舊
                {
                    CleanerActive = true;
                    BagCount = 1;
                }
                TrashAutoClean(CleanerActive);
            }
            TrashGenerate();
            StartCoroutine(Delay(delay));
        }
        else if (!isPressed)
        {  
            Trash = GameObject.FindGameObjectsWithTag("Clone");
            for (Count = 0; Count < Trash.Length; Count++) // 垃圾袋分類
            {
                Trash[Count].tag = "Bag" + BagCount;
                Trash[Count].name = "Bag" + BagCount + "Trash" + Count;
            }
            Debug.Log("Trash: " + Count + " Bag: " + BagCount);
            BagSave = BagCount;
            Count = 0;
        }
    }


    public void SwitchOcean() // 切換垃圾類型:海洋
    {
        i = 0;
        InitTrash();
        Debug.Log("Switch Ocean");
    }
    public void SwitchElectric() // 切換垃圾類型:電子
    {
        i = 1;
        InitTrash();
        Debug.Log("Switch Electric");
    }
    public void SwitchDisposable() // 切換垃圾類型:一次
    {
        i = 2;
        InitTrash();
        Debug.Log("Switch Disposable");
    }

    private void InitTrash() // 重置
    {
        for (int x = 1; x <= 9; x++)
        {
            Die = GameObject.FindGameObjectsWithTag("Bag" + x);
            for (int y = 0; y < Die.Length; y++)
            {
                Destroy(Die[y].gameObject);
            }
        }
        Total = 0;
        Count = 0;
        BagCount = 0;
        BagSave = 0;
        Cleaner = 1;
        CleanerActive = false;
        Debug.Log("Initial");
    }

    private void TrashGenerate() // 垃圾生成
    {

        if (i % 3 == 0)
        {
            Instantiate(Trash_Ocean[Random.Range(0, O)], Generate_Spot.position, Quaternion.identity);
        }
        else if (i % 3 == 1)
        {
            Instantiate(Trash_Electric[Random.Range(0,  E)], Generate_Spot.position, Quaternion.identity);
        }
        else if (i % 3 == 2)
        {
            Instantiate(Trash_Disposable[Random.Range(0, D)], Generate_Spot.position + Range, Quaternion.identity);
        }

    }

    private void TrashAutoClean(bool a) // 垃圾自動清除
    {
        if (a)
        {
            Die = GameObject.FindGameObjectsWithTag("Bag" + Cleaner);
            for (int x = 0; x < Die.Length; x++)
            {
                Destroy(Die[x].gameObject);
            }
            Cleaner++;
            if (Cleaner > 9)
            {
                Cleaner = 1;
            }
        }
    }

    private void TrashTotal(int t) // 判斷垃圾總數是否該進入下一關
    {
        if(t >= 150)
        {
            for (int x = 1; x <= 9; x++)
            {
                NotDying = GameObject.FindGameObjectsWithTag("Bag" + x);
                for (int y = 0; y < NotDying.Length; y++)
                {
                    NotDying[y].tag = "Score";
                }
            }
            NotDying = GameObject.FindGameObjectsWithTag("Clone");
            for (int y = 0; y < NotDying.Length; y++)
            {
                NotDying[y].tag = "Score";
            }
            Score = GameObject.FindGameObjectsWithTag("Score");
            for (int x = 1; x <= 9; x++)
            {
                NotDying = GameObject.FindGameObjectsWithTag("Bag" + x);
                for (int z = 0; z < Score.Length; z++)
                {
                    DontDestroyOnLoad(Score[z]);
                }
            }
            
            SceneManager.LoadScene("Pipe");            
        }
    }

    void InputTF()
    {
        if(Connect.readi >= 4)
        {
            isPressed = true;
            delay = Connect.readi;
        }
        else
        {
            isPressed = false;
        }
    }

    public IEnumerator Delay(float time)
    {
        yield return new WaitForSeconds(Gspeed / time);
        Debug.Log("Delay : " + Gspeed / time);
    }
}