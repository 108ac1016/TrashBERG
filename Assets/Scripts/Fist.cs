﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fist : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "ModelA")
        {
            GameObject Flat;
            Flat = other.gameObject.transform.parent.Find("Pressed").gameObject;
            Flat.transform.position = other.gameObject.transform.position;
            Flat.transform.rotation = other.gameObject.transform.rotation;
            Flat.SetActive(true);
            other.gameObject.SetActive(false);
            //other.gameObject.SetActive(false);
        }  
    }
}
