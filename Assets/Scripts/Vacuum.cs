﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vacuum : MonoBehaviour
{
    public GameObject Hole;
    public GameObject Cam;
    public GameObject GS_Two;
    public int Limit;

    GameObject[] Trash_O, Trash_P;
    Animation Cam_Anim;
    int Count, Long;
    bool Stop;

    // Start is called before the first frame update
    void Start()
    {
        Cam_Anim = Cam.GetComponent<Animation>();
        Count = 0;
        Stop = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!Stop)
        {
            if (Count >= Limit)
            {
                Pipe.stage = 2;
                Trash_O = GameObject.FindGameObjectsWithTag("ModelA");
                Trash_P = GameObject.FindGameObjectsWithTag("ModelB");
                Cam_Anim.Play("CamFirst");
                
                for(int i = 0; i < Trash_O.Length; i++)
                {
                    Trash_O[i].GetComponent<Rigidbody>().isKinematic = false;
                    Trash_O[i].transform.position = GS_Two.transform.position;
                }
                for (int i = 0; i < Trash_P.Length; i++)
                {
                    Trash_P[i].GetComponent<Rigidbody>().isKinematic = false;
                    Trash_P[i].transform.position = GS_Two.transform.position;
                }
                Stop = true;
            }
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "ModelA" || other.gameObject.tag == "ModelB")
        {
            other.gameObject.GetComponent<Rigidbody>().isKinematic = true;
            other.gameObject.transform.position = Vector3.Lerp(other.transform.position, Hole.transform.position, 0.1f);
            Count++;
        }
    }
}
