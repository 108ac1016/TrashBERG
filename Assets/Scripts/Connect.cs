﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using System.IO.Ports;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Connect : MonoBehaviour
{
    // public Text Trash_Type, Press_Value, Total_Value;
    SerialPort serial1;
    string read;
    public static int readi, final;
    GameObject ScriptManager, Origin;
    int total;

    void Start()
    {
        Origin = GameObject.FindGameObjectWithTag("OldConnect");
        if(Origin == null)
        {
            serial1 = new SerialPort("COM3", 9600);
            serial1.Open();
            this.tag = "OldConnect";
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(Origin);
        }
        ScriptManager = GameObject.FindGameObjectWithTag("Script");
        total = 0;
    }

    void Update()
    {
        if (serial1.IsOpen)
        {
            read = serial1.ReadLine();

            if (SceneManager.GetActiveScene().name == "Main")
            {
                if (read == "1")
                {
                    if (SceneManager.GetActiveScene().name == "Main")
                    {
                        final = 1;
                        ScriptManager.GetComponent<Generation>().SwitchOcean();
                        // Trash_Type.text = "Ocean";
                    }
                }
                else if (read == "2")
                {
                    if (SceneManager.GetActiveScene().name == "Main")
                    {
                        final = 2;
                        ScriptManager.GetComponent<Generation>().SwitchElectric();
                        // Trash_Type.text = "Electronic";
                    }

                }
                else if (read == "3")
                {
                    if (SceneManager.GetActiveScene().name == "Main")
                    {
                        final = 3;
                        ScriptManager.GetComponent<Generation>().SwitchDisposable();
                        // Trash_Type.text = "Disposable";
                    }
                }
                else
                {
                    // Press_Value.text = read;
                }
            }
        }

        try
        {
            readi = Int32.Parse(read);
        }
        catch(FormatException e)
        {
            Console.WriteLine(e.Message);
        }
        total += readi;
        // Total_Value.text = total.ToString();
        Debug.Log(readi);
    }
}
