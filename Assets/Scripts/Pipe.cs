﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pipe : MonoBehaviour
{
    public Transform GS;
    public GameObject[] trash;
    public GameObject Fist, Head;
    public float Mag;
    public GameObject Cam;
    public GameObject End;
    static public int Total;

    GameObject[] Trash,Die;
    GameObject Con;
    Animation Anim;
    GameObject ScaleGrow;
    Vector3 RandomV;
    int random, Burn;
    static public int stage;
    Animation Cam_Anim;
    bool isPressed;

    
    void Start()
    {
        //Trash = GameObject.FindGameObjectsWithTag("Score");
        stage = 1;
        Burn = 0;
        Total = 100;
        RandomV = new Vector3(Random.Range(0, 5), 0, Random.Range(0, 5));
        isPressed = false;
        MoveTrash();
        Cam_Anim = Cam.GetComponent<Animation>();
        //End.SetActive(false);
    }

    void Update()
    {
        FistDown_A(); 
    }

    private void FistDown_A()
    {
        if (stage == 1)
        {
            if (Connect.readi >= 4)
            {
                Anim = Fist.GetComponent<Animation>();
                Anim.CrossFade("Fist_Anim");
            }
        }
        else if (stage == 2)
        {
            if (Connect.readi >= 4)
            {
                Anim = Head.GetComponent<Animation>();
                Anim.CrossFade("Head_Anim");
                BurnTHEWitch.Stop = false;
                Burn++;
                if (Burn == 25)
                {
                    stage = 3;
                    Cam_Anim.Play("CamSecond");
                }
            }
        }
        else if(stage == 3)
        {
            if (Hand.One && Hand.Two && Hand.Three)
            {
                stage = 4;
                Cam_Anim.Play("CamThird");
            }
        }
        else if(stage == 4)
        {
            
        }
        else if(stage == 5)
        {
            End.SetActive(true);
        }
    }

    private void MoveTrash()
    {
        for (int x = 1; x <= 9; x++)
        {
            Trash = GameObject.FindGameObjectsWithTag("Score");
            for (int y = 0; y < Trash.Length; y++)
            {
                Trash[y].transform.position = GS.position;
            }
        }
    }

    public void Generate_B()
    {
        isPressed = true;
        Instantiate(trash[Random.Range(0, trash.Length)], GS.position, Quaternion.identity);
    }

    public void ReStart()
    {
        Con = GameObject.FindGameObjectWithTag("OldConnect");
        Destroy(Con);
        Die = GameObject.FindGameObjectsWithTag("Score");
        for(int i= 0; i < Die.Length; i++)
        {
            Destroy(Die[i]);
        }
        SceneManager.LoadScene("Main");
    }
}
