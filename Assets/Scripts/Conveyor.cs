﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Conveyor : MonoBehaviour
{
    public float speed;
    Rigidbody rBody;
    public Vector3 Opposite_direction;

    // Start is called before the first frame update
    void Start()
    {
        rBody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        Vector3 pos = rBody.position;
        rBody.position += -Opposite_direction * speed * Time.deltaTime;
        rBody.MovePosition(pos);
    }
}
